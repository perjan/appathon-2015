#import "CardsInstallmentPlan.h"

@implementation CardsInstallmentPlan

/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper
{
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"installmentPlanID": @"installmentPlanID", @"installmentsAmount": @"installmentsAmount", @"installmentsNumber": @"installmentsNumber", @"totalAmount": @"totalAmount", @"movementsNumber": @"movementsNumber", @"movements": @"movements", @"installments": @"installments", @"averageFees": @"averageFees", @"totalFees": @"totalFees", @"feesAmount": @"feesAmount", @"taeg": @"taeg", @"teg": @"teg", @"firstInstallment": @"firstInstallment", @"lastInstallment": @"lastInstallment", @"residualDebit": @"residualDebit" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
  NSArray *optionalProperties = @[@"teg", ];

  if ([optionalProperties containsObject:propertyName]) {
    return YES;
  }
  else {
    return NO;
  }
}

/**
 * Gets the string presentation of the object.
 * This method will be called when logging model object using `NSLog`.
 */
- (NSString *)description {
    return [[self toDictionary] description];
}

@end

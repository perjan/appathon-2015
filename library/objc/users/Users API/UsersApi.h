#import <Foundation/Foundation.h>
#import "UsersGetUserInfoResponse.h"
#import "UsersPutUserInfoResponse.h"
#import "UsersPutUserInfoRequest.h"
#import "UsersObject.h"
#import "UsersApiClient.h"


/**
 * NOTE: This class is auto generated by the swagger code generator program. 
 * https://github.com/swagger-api/swagger-codegen 
 * Do not edit the class manually.
 */

@interface UsersApi: NSObject

@property(nonatomic, assign)UsersApiClient *apiClient;

-(instancetype) initWithApiClient:(UsersApiClient *)apiClient;
-(void) addHeader:(NSString*)value forKey:(NSString*)key;
-(unsigned long) requestQueueSize;
+(UsersApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key;
///
///
/// Retrieve User Info
/// 
///
/// @param userId User Identifier
/// 
///
/// @return UsersGetUserInfoResponse*
-(NSNumber*) getUserInfoWithCompletionBlock :(NSString*) userId 
    
    completionHandler: (void (^)(UsersGetUserInfoResponse* output, NSError* error))completionBlock;
    


///
///
/// Update User Info
/// 
///
/// @param userId User Identifier
/// @param newInfo Info to be updated
/// 
///
/// @return UsersPutUserInfoResponse*
-(NSNumber*) updateUserInfoWithCompletionBlock :(NSString*) userId 
     newInfo:(UsersPutUserInfoRequest*) newInfo 
    
    completionHandler: (void (^)(UsersPutUserInfoResponse* output, NSError* error))completionBlock;
    



@end

//
//  PDPaymentManager.swift
//  TeenWallet
//
//  Created by Perjan Duro on 07/11/15.
//
//

import Foundation


class PDPaymentManager: NSObject {
    let paymentAPI = PaymentsApi()
    let accountsAPI = AccountsApi()
    
    func demoSender() -> PaymentsSender {
        let sender = PaymentsSender()
        sender.accountId = "82873319970"
        sender.accountIdType = "AccountNumber"
        return sender
    }
    
    func demoRecipient() -> PaymentsRecipient {
        let recipient = PaymentsRecipient()
        recipient.accountId = "IT000001234234DSFD34D"
        recipient.accountIdType = "IBAN"
        recipient.bankCode = "CHASUS33XXX"
        recipient.name = "Federico Bianchi"
        return recipient
    }
    
    func makePayment(amount:NSNumber, currency:String, recipient:PaymentsRecipient, sender:PaymentsSender, completionBlock:(success:Bool, draftTransactionId:String?) -> Void) {
        
        let request = PaymentsCreateDomesticTransferRequest()
        request.amount = amount
        request.cause = "Domestic Transfer"
        request.currency = currency
        request.immediate = NSNumber(bool: true)
        request.requestedExecutionDate = NSDate()
        
        request.recipient = recipient
        request.sender = sender
        
        paymentAPI.createDomesticPaymentWithCompletionBlock(request, userId: kUserId) { (response:PaymentsDomesticTransferResponse!, error:NSError!) -> Void in
            if error == nil {
                print(response.description)
                completionBlock(success: true, draftTransactionId: response.transactionId)
            }
            else {
                print(error)
                completionBlock(success: false, draftTransactionId: nil)
            }
        }
    }
    
    func recipientFromAccount(accountId: String, completionBlock:(success:Bool, recipient:PaymentsRecipient) -> Void) {
        accountsAPI.getAccountDetailWithCompletionBlock(accountId, userId: nil) { (response:AccountsGetAccountResponse!, error:NSError!) -> Void in
            if error == nil {
                print(response.account.name)
                print(response)
                let recipient = PaymentsRecipient()
                recipient.accountId = response.account.iBAN
                recipient.accountIdType = "IBAN"
                recipient.bankCode = response.account.bank.code
                recipient.name = response.account.owner.name
                
                completionBlock(success: true, recipient: recipient)
            }
            else {
                print(error)
            }
        }
    }
    
    func verifyPaymentSMS(smsCode:String, transactionId:String, completionBlock:(success:Bool) -> Void) {
        let request = PaymentsSignDomesticTransfer()
        request.methodCode = "SMS"

        let requiredSignValues = PaymentsRequiredSignValues()
        requiredSignValues.code = smsCode
        request.requiredSignValues = requiredSignValues
        
        paymentAPI.postPaymentSignWithCompletionBlock(transactionId, body: request, userId: nil) { (response:PaymentsDomesticTransferStatus!, error:NSError!) -> Void in
            if error == nil {
                print(response)
                completionBlock(success: true)
            }
            else {
                print(error)
                completionBlock(success: false)
//                PaymentsResponseObject
            }
        }
    }
    
//    func senderFromAccount(account:AccountsAccount, completionBlock:(success:Bool, sender:PaymentsSender) -> Void) {
//    
//    }
}

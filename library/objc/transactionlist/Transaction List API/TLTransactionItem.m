#import "TLTransactionItem.h"

@implementation TLTransactionItem

/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper
{
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"transactionID": @"transactionID", @"description": @"_description", @"currency": @"currency", @"amount": @"amount", @"type": @"type", @"bookingDate": @"bookingDate", @"valueDate": @"valueDate", @"purpose": @"purpose", @"statusCode": @"statusCode", @"statusDescription": @"statusDescription", @"account": @"account", @"detail": @"detail", @"meta": @"meta" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
  NSArray *optionalProperties = @[];

  if ([optionalProperties containsObject:propertyName]) {
    return YES;
  }
  else {
    return NO;
  }
}

/**
 * Gets the string presentation of the object.
 * This method will be called when logging model object using `NSLog`.
 */
- (NSString *)description {
    return [[self toDictionary] description];
}

@end

#import "PaymentsUpdateDomesticTransferResponse.h"

@implementation PaymentsUpdateDomesticTransferResponse

/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper
{
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"transactionId": @"transactionId", @"amount": @"amount", @"currency": @"currency", @"cause": @"cause", @"immediate": @"immediate", @"requestedExecutionDate": @"requestedExecutionDate", @"chargesType": @"chargesType", @"chargesAmount": @"chargesAmount", @"recipient": @"recipient", @"sender": @"sender", @"statusCode": @"statusCode", @"statusMessage": @"statusMessage" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
  NSArray *optionalProperties = @[@"transactionId", @"amount", @"currency", @"cause", @"immediate", @"requestedExecutionDate", @"chargesType", @"chargesAmount", @"recipient", @"sender", @"statusCode", @"statusMessage"];

  if ([optionalProperties containsObject:propertyName]) {
    return YES;
  }
  else {
    return NO;
  }
}

/**
 * Gets the string presentation of the object.
 * This method will be called when logging model object using `NSLog`.
 */
- (NSString *)description {
    return [[self toDictionary] description];
}

@end

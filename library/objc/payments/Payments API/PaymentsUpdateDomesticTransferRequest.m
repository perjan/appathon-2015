#import "PaymentsUpdateDomesticTransferRequest.h"

@implementation PaymentsUpdateDomesticTransferRequest

/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper
{
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"amount": @"amount", @"currency": @"currency", @"cause": @"cause", @"immediate": @"immediate", @"requestedExecutionDate": @"requestedExecutionDate", @"recipient": @"recipient", @"sender": @"sender" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
  NSArray *optionalProperties = @[@"amount", @"currency", @"cause", @"immediate", @"requestedExecutionDate", @"recipient", @"sender"];

  if ([optionalProperties containsObject:propertyName]) {
    return YES;
  }
  else {
    return NO;
  }
}

/**
 * Gets the string presentation of the object.
 * This method will be called when logging model object using `NSLog`.
 */
- (NSString *)description {
    return [[self toDictionary] description];
}

@end

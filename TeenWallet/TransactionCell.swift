//
//  TransactionCell.swift
//  TeenWallet
//
//  Created by Perjan Duro on 07/11/15.
//
//

import UIKit

class TransactionCell: UITableViewCell {

    @IBOutlet weak var transactionImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

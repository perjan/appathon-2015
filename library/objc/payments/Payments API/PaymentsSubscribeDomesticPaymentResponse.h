#import <Foundation/Foundation.h>
#import "PaymentsObject.h"

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */



@protocol PaymentsSubscribeDomesticPaymentResponse
@end

@interface PaymentsSubscribeDomesticPaymentResponse : PaymentsObject


@property(nonatomic) NSString* response;

@property(nonatomic) NSString* status;

@end

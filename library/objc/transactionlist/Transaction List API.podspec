#
# Be sure to run `pod lib lint Transaction List API.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = "Transaction List API"
    s.version          = "1.0.0"

    s.summary          = ""
    s.description      = <<-DESC
                         
                         DESC

    s.platform     = :ios, '7.0'
    s.requires_arc = true

    s.framework    = 'SystemConfiguration'

    s.source_files = 'Transaction List API/**/*'
    s.public_header_files = 'Transaction List API/**/*.h'

    s.dependency 'AFNetworking', '~> 2.3'
    s.dependency 'ISO8601', '~> 0.3'
end


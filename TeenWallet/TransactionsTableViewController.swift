//
//  TransactionsTableViewController.swift
//  TeenWallet
//
//  Created by Perjan Duro on 07/11/15.
//
//

import UIKit

class TransactionsTableViewController: UITableViewController {
    
    var dataSource = [TLTransactionItem]()
    let nf = NSNumberFormatter()
    let df = NSDateFormatter()
    
    let greenColor = UIColor(rgba: kColorGreen)
    let redColor = UIColor(rgba: kColorRed)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nf.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        df.dateFormat = "yyyy-MM-dd"
        
        let pdTransactionManager = PDTransactionManager()
        pdTransactionManager.allTransactions { (success, transactions) -> Void in
            if success {
                self.dataSource = transactions!
                self.tableView.reloadData()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return dataSource.count
    }

    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! TransactionCell

        let transaction = dataSource[indexPath.row]
        cell.titleLabel.text = transaction._description
        cell.subtitleLabel.text = df.stringFromDate(transaction.valueDate)
        nf.currencyCode = transaction.currency
        cell.amountLabel.text = nf.stringFromNumber(transaction.amount)
        
        if transaction.amount.doubleValue > 0 {
            cell.amountLabel.textColor = greenColor
        }
        else {
            cell.amountLabel.textColor = redColor
        }

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

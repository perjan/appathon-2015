#import "BFMApi.h"
#import "BFMQueryParamCollection.h"
#import "BFMCategories.h"
#import "BFMCredits.h"
#import "BFMDailyTimelineItems.h"
#import "BFMMonthlyTimelineItems.h"
#import "BFMCreditDetail.h"


@interface BFMApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation BFMApi

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        BFMConfiguration *config = [BFMConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[BFMApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(BFMApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(BFMApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {
    static BFMApi* singletonAPI = nil;

    if (singletonAPI == nil) {
        singletonAPI = [[BFMApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [BFMApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Get account categories by BFM Big Data engine
/// 
///  @param accountId id of account
///
///  @param userId User unique identifier
///
///  @returns BFMCategories*
///
-(NSNumber*) getCategoriesWithCompletionBlock: (NSString*) accountId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(BFMCategories* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'accountId' is set
    if (accountId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `accountId` when calling `getCategories`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bfm/v1/accounts/{accountId}/categories"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (accountId != nil) {
        pathParams[@"accountId"] = accountId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BFMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BFMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BFMCategories*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BFMCategories*)data, error);
              }
          ];
}

///
/// Get credits from account
/// 
///  @param accountId id of account
///
///  @param userId User unique identifier
///
///  @returns BFMCredits*
///
-(NSNumber*) getAccountCreditsWithCompletionBlock: (NSString*) accountId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(BFMCredits* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'accountId' is set
    if (accountId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `accountId` when calling `getAccountCredits`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bfm/v1/accounts/{accountId}/credits"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (accountId != nil) {
        pathParams[@"accountId"] = accountId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BFMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BFMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BFMCredits*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BFMCredits*)data, error);
              }
          ];
}

///
/// Get daily transactions
/// 
///  @param accountId id of account
///
///  @param dateFrom selects transactions occurred after the date specified
///
///  @param dateTo selects transactions occurred before the date specified
///
///  @param categoryIdList selects transactions that contain the categoryId specified in the list. The values are comma separated.
///
///  @param userId User unique identifier
///
///  @returns BFMDailyTimelineItems*
///
-(NSNumber*) getDailyTransactionsWithCompletionBlock: (NSString*) accountId
         dateFrom: (NSDate*) dateFrom
         dateTo: (NSDate*) dateTo
         categoryIdList: (NSString*) categoryIdList
         userId: (NSString*) userId
        
        completionHandler: (void (^)(BFMDailyTimelineItems* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'accountId' is set
    if (accountId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `accountId` when calling `getDailyTransactions`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bfm/v1/accounts/{accountId}/timeline/daily"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (accountId != nil) {
        pathParams[@"accountId"] = accountId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(dateFrom != nil) {
        
        queryParams[@"dateFrom"] = dateFrom;
    }
    if(dateTo != nil) {
        
        queryParams[@"dateTo"] = dateTo;
    }
    if(categoryIdList != nil) {
        
        queryParams[@"categoryIdList"] = categoryIdList;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BFMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BFMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BFMDailyTimelineItems*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BFMDailyTimelineItems*)data, error);
              }
          ];
}

///
/// Get monthly transactions
/// 
///  @param accountId id of account
///
///  @param categoryIdList selects transactions that contain the categoryId specified in the list. The values are comma separated.
///
///  @param dateFrom selects transactions occurred after the date specified
///
///  @param dateTo selects transactions occurred before the date specified
///
///  @param userId User unique identifier
///
///  @returns BFMMonthlyTimelineItems*
///
-(NSNumber*) getMonthlyTransactionsWithCompletionBlock: (NSString*) accountId
         categoryIdList: (NSString*) categoryIdList
         dateFrom: (NSDate*) dateFrom
         dateTo: (NSDate*) dateTo
         userId: (NSString*) userId
        
        completionHandler: (void (^)(BFMMonthlyTimelineItems* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'accountId' is set
    if (accountId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `accountId` when calling `getMonthlyTransactions`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bfm/v1/accounts/{accountId}/timeline/monthly"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (accountId != nil) {
        pathParams[@"accountId"] = accountId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(categoryIdList != nil) {
        
        queryParams[@"categoryIdList"] = categoryIdList;
    }
    if(dateFrom != nil) {
        
        queryParams[@"dateFrom"] = dateFrom;
    }
    if(dateTo != nil) {
        
        queryParams[@"dateTo"] = dateTo;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BFMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BFMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BFMMonthlyTimelineItems*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BFMMonthlyTimelineItems*)data, error);
              }
          ];
}

///
/// Get all credits
/// 
///  @param userId User unique identifier
///
///  @returns BFMCredits*
///
-(NSNumber*) getCreditsWithCompletionBlock: (NSString*) userId
        
        completionHandler: (void (^)(BFMCredits* output, NSError* error))completionBlock { 
        

    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bfm/v1/credits"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BFMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BFMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BFMCredits*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BFMCredits*)data, error);
              }
          ];
}

///
/// Get credit details
/// 
///  @param creditId id of credit
///
///  @param userId User unique identifier
///
///  @returns BFMCreditDetail*
///
-(NSNumber*) getCreditDetailsWithCompletionBlock: (NSString*) creditId
         userId: (NSString*) userId
        
        completionHandler: (void (^)(BFMCreditDetail* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'creditId' is set
    if (creditId == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `creditId` when calling `getCreditDetails`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/bfm/v1/credits/{creditId}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (creditId != nil) {
        pathParams[@"creditId"] = creditId;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [BFMApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [BFMApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"BFMCreditDetail*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((BFMCreditDetail*)data, error);
              }
          ];
}



@end

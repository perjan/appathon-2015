//
//  PDInstallment.swift
//  TeenWallet
//
//  Created by Perjan Duro on 07/11/15.
//
//

import Foundation

class PDInstallment: NSObject {
    let cardApi = CardsApi()
    
    func createInstallmentRequest(installmentAmount:NSNumber, cardID:String, completionBlock:(success:Bool) -> Void) {
        let request = CardsNewInstallmentPlan()
        request.installmentsNumber = NSNumber(integer: 1)
        
        var movements = [CardsActivity]()
        
        let activity = CardsActivity()
        activity.activityID = NSNumber(integer: 2039)
        activity.amount = installmentAmount
        
//        let df = NSDateFormatter()
//        df.dateFormat = "yyyy-MM-dd"
        
//        let valueDate = NSDate()
//        let operationDate = NSDate()
        
        activity.valueDate = NSDate()
        activity.operationDate = NSDate()
        
        movements.append(activity)
        request.movements = movements
        
        self.cardApi.createCardInstallmentPlanWithCompletionBlock("false", cardID: cardID, body: request, userId: kUserId) { (installmentPlan:CardsInstallmentPlan!, error:NSError!) -> Void in
            if error == nil {
                print(installmentPlan)
                completionBlock(success: true)
            }
            else {
                print("error")
                completionBlock(success: false)
            }
        }
        
    }
}
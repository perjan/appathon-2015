//
//  PDTransactionManager.swift
//  TeenWallet
//
//  Created by Perjan Duro on 07/11/15.
//
//

import Foundation

class PDTransactionManager: NSObject {
    let transactionAPI = TransactionsListApi()
    
    func allTransactions(completionBlock:(success:Bool, transactions:[TLTransactionItem]?) -> Void) {
        let df = NSDateFormatter()
        df.dateFormat = "yyyy-MM-dd"
        
        let valueDateFrom = df.dateFromString("2015-10-01")!
        let valueDateTo = df.dateFromString("2015-11-11")!
        
        transactionAPI.getUserTransactionsWithCompletionBlock(nil, endDate: valueDateTo, category: nil, startDate: valueDateFrom, userId: kUserId) { (response:TLGetTransactionListResponse!, error:NSError!) -> Void in
            if error == nil {
                print(response)
                completionBlock(success: true, transactions: response.transactions as? [TLTransactionItem])
            }
            else {
                print(error)
                completionBlock(success: false, transactions: nil)
                // TLResponseObject
            }
        }
    }
}

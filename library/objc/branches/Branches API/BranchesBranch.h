#import <Foundation/Foundation.h>
#import "BranchesObject.h"

/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

#import "BranchesLobbyHours.h"
#import "BranchesAddress.h"
#import "BranchesContacts.h"
#import "BranchesService.h"


@protocol BranchesBranch
@end

@interface BranchesBranch : BranchesObject


@property(nonatomic) NSString* _id;

@property(nonatomic) NSString* _description;

@property(nonatomic) BranchesAddress* address;

@property(nonatomic) BranchesLobbyHours* lobbyHours;

@property(nonatomic) BranchesContacts* contacts;

@property(nonatomic) NSString* bankCode;

@property(nonatomic) NSString* branchCode;

@property(nonatomic) NSString* branchNumber;

@property(nonatomic) NSArray<BranchesService>* services;

@end

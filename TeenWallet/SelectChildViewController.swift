//
//  SelectChildViewController.swift
//  TeenWallet
//
//  Created by Perjan Duro on 07/11/15.
//
//

import UIKit

class SelectChildViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "john" || segue.identifier == "anna" {
            let destVC = segue.destinationViewController as! RequestMoneyViewController
            destVC.recipientName = segue.identifier!
        }
    }
    

}

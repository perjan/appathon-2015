#import "BFMCreditDetail.h"

@implementation BFMCreditDetail

/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper
{
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"id": @"_id", @"type": @"type", @"code": @"code", @"associatedAccount": @"associatedAccount", @"creditGranted": @"creditGranted", @"creditAvailable": @"creditAvailable", @"currency": @"currency", @"createDate": @"createDate", @"cashflowDetails": @"cashflowDetails", @"advanceDetails": @"advanceDetails", @"promiscuousDetails": @"promiscuousDetails", @"loanDetails": @"loanDetails" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
  NSArray *optionalProperties = @[@"cashflowDetails", @"advanceDetails", @"promiscuousDetails", @"loanDetails"];

  if ([optionalProperties containsObject:propertyName]) {
    return YES;
  }
  else {
    return NO;
  }
}

/**
 * Gets the string presentation of the object.
 * This method will be called when logging model object using `NSLog`.
 */
- (NSString *)description {
    return [[self toDictionary] description];
}

@end

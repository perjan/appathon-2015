//
//  PDAlertViewController.swift
//  ClearTime
//
//  Created by Perjan Duro on 16/02/15.
//  Copyright (c) 2015 Perjan Duro. All rights reserved.
//

import UIKit

@objc protocol PDAlertViewControllerDelegate {
    optional func alertViewModal(alertView: PDAlertViewController, clickedButtonAtIndex buttonIndex: Int)
}

class PDAlertViewController: UIViewController {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet var proceedButton: UIButton!
    
    var borderColor: UIColor! = UIColor()
    var titleText: String?
    var subtitleText: String?
    var image: UIImage!
    var imageURL: NSURL!
    var buttonTitle: String?
    var delegate:PDAlertViewControllerDelegate? = nil
    
    class func className () -> String {
        return "PDAlertViewController"
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    convenience init() {
        self.init()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if titleText != nil {
            titleLabel.text = titleText
        }
        
        if subtitleText != nil {
            subtitleLabel.text = subtitleText
        }
        
        if buttonTitle != nil {
            proceedButton.setTitle(buttonTitle, forState: UIControlState.Normal)
        }
        
        
        imageView.layer.cornerRadius = CGRectGetHeight(imageView.frame) / 2
        
        self.view.layer.borderWidth = 1
        self.view.layer.cornerRadius = 8
    }
    
    init(title:String) {
        super.init(nibName: PDAlertViewController.className(), bundle: NSBundle.mainBundle())
        self.setPopinTransitionStyle(BKTPopinTransitionStyle.SpringySlide)
        let blurParameters = BKTBlurParameters()
        blurParameters.tintColor = UIColor.blackColor()
        blurParameters.alpha = 0.8
        self.setBlurParameters(blurParameters)
        self.setPopinOptions([BKTPopinOption.DisableAutoDismiss, BKTPopinOption.BlurryDimmingView])
        self.setPopinTransitionDirection(BKTPopinTransitionDirection.Bottom)
        
        self.titleText = title
        self.borderColor = UIColor(rgba: kColorOrange)
        self.buttonTitle = ""
    }
    
    init(longAlert title:String?, subtitle:String?, image:UIImage?) {
        super.init(nibName: "PDAlertViewControllerLong", bundle: NSBundle.mainBundle())
        self.setPopinTransitionStyle(BKTPopinTransitionStyle.SpringySlide)
        let blurParameters = BKTBlurParameters()
        blurParameters.tintColor = UIColor.blackColor()
        blurParameters.alpha = 0.8
        self.setBlurParameters(blurParameters)
        self.setPopinOptions([BKTPopinOption.DisableAutoDismiss, BKTPopinOption.BlurryDimmingView])
        self.setPopinTransitionDirection(BKTPopinTransitionDirection.Bottom)
        
        if (title != nil) {
            self.titleText = title
        }
        if (subtitle != nil) {
            self.subtitleText = subtitle
        }
        
        if (image != nil) {
            self.image = image
        }
        
        self.borderColor = UIColor(rgba: kColorOrange)
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }

    @IBAction func proceedButtonDidPress(sender: AnyObject) {
        delegate?.alertViewModal!(self, clickedButtonAtIndex: 1)
    }
    @IBAction func cancelButtonDidPress(sender: AnyObject) {
        delegate?.alertViewModal!(self, clickedButtonAtIndex: 0)
    }
}

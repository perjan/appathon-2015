#import "BFMLoan.h"

@implementation BFMLoan

/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper
{
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"description": @"_description", @"issueDate": @"issueDate", @"expirationDate": @"expirationDate", @"totalResidualDebit": @"totalResidualDebit", @"disbursedAmount": @"disbursedAmount", @"installmentsNumber": @"installmentsNumber", @"installmentAmount": @"installmentAmount", @"fee": @"fee", @"currentInstallmentNumber": @"currentInstallmentNumber", @"lastInstallmentPaidDate": @"lastInstallmentPaidDate", @"nextInstallmentDate": @"nextInstallmentDate", @"installments": @"installments" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
  NSArray *optionalProperties = @[];

  if ([optionalProperties containsObject:propertyName]) {
    return YES;
  }
  else {
    return NO;
  }
}

/**
 * Gets the string presentation of the object.
 * This method will be called when logging model object using `NSLog`.
 */
- (NSString *)description {
    return [[self toDictionary] description];
}

@end

#import "CardsApi.h"
#import "CardsQueryParamCollection.h"
#import "CardsCardsList.h"
#import "CardsCard.h"
#import "CardsCardAgreementsPut.h"
#import "CardsActivitiesList.h"
#import "CardsInstallmentPlansList.h"
#import "CardsNewInstallmentPlan.h"
#import "CardsInstallmentPlan.h"
#import "CardsDeleteInstallmentPlan.h"


@interface CardsApi ()
    @property (readwrite, nonatomic, strong) NSMutableDictionary *defaultHeaders;
@end

@implementation CardsApi

static CardsApi* singletonAPI = nil;

#pragma mark - Initialize methods

- (id) init {
    self = [super init];
    if (self) {
        CardsConfiguration *config = [CardsConfiguration sharedConfig];
        if (config.apiClient == nil) {
            config.apiClient = [[CardsApiClient alloc] init];
        }
        self.apiClient = config.apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

- (id) initWithApiClient:(CardsApiClient *)apiClient {
    self = [super init];
    if (self) {
        self.apiClient = apiClient;
        self.defaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

+(CardsApi*) apiWithHeader:(NSString*)headerValue key:(NSString*)key {

    if (singletonAPI == nil) {
        singletonAPI = [[CardsApi alloc] init];
        [singletonAPI addHeader:headerValue forKey:key];
    }
    return singletonAPI;
}

+(CardsApi*) sharedAPI {

    if (singletonAPI == nil) {
        singletonAPI = [[CardsApi alloc] init];
    }
    return singletonAPI;
}

-(void) addHeader:(NSString*)value forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(void) setHeaderValue:(NSString*) value
           forKey:(NSString*)key {
    [self.defaultHeaders setValue:value forKey:key];
}

-(unsigned long) requestQueueSize {
    return [CardsApiClient requestQueueSize];
}

#pragma mark - Api Methods

///
/// Retrieves user's cards list
/// <p style=\"font-size: 14px;\">Retrieves the cards list for the current user; a summary is returned, for the detail of each card use the other API Method \"GET Card by Id\". Each card has an account associated. It's possible to filter the result set based on the following filters: account number, card status, card product type. </p> <h2>Optional Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">accountnumber</td>          <td align=\"left\">Querystring parameter to get the cards of the account specified.</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">status</td>          <td align=\"left\">Querystring parameter to get the cards with the status specified.</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">type</td>          <td align=\"left\">Querystring parameter to get the cards of the type specified.</td>       </tr>    </tbody> </table> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">cardId</td>          <td align=\"left\">Card PAN code</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">name</td>          <td align=\"left\">card name (e.g. Flexia)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">expirationDate</td>          <td align=\"left\">card expiration date</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a get cards API call with Java and Objective-C SDK.</p>\n<div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java7\">\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.CardsList;\n\npublic class GetCardsExample {\n	\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n		\n		try {\n			CardsList cardsList = cardsApi.getCards(\"ACTIVE\", \"VISA\", null);\n			System.out.println(cardsList);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c7\">\n#import <CardsApi.h>\n  \n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n\nCardsApi *cardsApi = [[CardsApi alloc]init];   \n\n[cardsApi getCardsWithCompletionBlock:@\"ACTIVE\" type:@\"VISA\" accountnumber:nil userId:nil completionHandler:^(CardsCardsList *output, NSError *error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>GET /cards/v1?status=ACTIVE&type=VISA</p>
///  @param status Card status filter
///
///  @param type Card product type filter
///
///  @param accountnumber Card account number filter
///
///  @param userId User unique identifier
///
///  @returns CardsCardsList*
///
-(NSNumber*) getCardsWithCompletionBlock: (NSString*) status
         type: (NSString*) type
         accountnumber: (NSString*) accountnumber
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsCardsList* output, NSError* error))completionBlock { 
        

    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(status != nil) {
        
        queryParams[@"status"] = status;
    }
    if(type != nil) {
        
        queryParams[@"type"] = type;
    }
    if(accountnumber != nil) {
        
        queryParams[@"accountnumber"] = accountnumber;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsCardsList*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsCardsList*)data, error);
              }
          ];
}

///
/// Retrieves info about a specific credit card
/// <p style=\"font-size: 14px;\">Retrieves the detail of a specific credit card. A card has an account associated, holds the billing information for the installment associated to it (if the card is of 'Revolving' type),  the monthly spending limits and status information (e.g. active, expired) </p> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">cardId</td>          <td align=\"left\">Card PAN code</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">name</td>          <td align=\"left\">card name (e.g. Flexia)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">issueDate</td>          <td align=\"left\">Card issue date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">expirationDate</td>          <td align=\"left\">Card expiration date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">plafond</td>          <td align=\"left\">Card plafond amount. If the card is prepaid the plafond is the amount ceiling of the card, else is the monthly expense ceiling</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">balance</td>          <td align=\"left\">Card balance amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">holderName</td>          <td align=\"left\">Card holder full name</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">holderIdentifier</td>          <td align=\"left\">Card holder unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">productType</td>          <td align=\"left\">Card product type (e.g. Credit Card)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">status</td>          <td align=\"left\">Card status (e.g. ACTIVE, CANCELLED)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">billingType</td>          <td align=\"left\">Card repay type (e.g. Balance, Revolving)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">billingDay</td>          <td align=\"left\">Card billing day</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentAmount</td>          <td align=\"left\">Card installment amount  if the card billing type is REVOLVING</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a get credit card API call with Java and Objective-C SDK.</p>\n<div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java3\">\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.Card;\n\npublic class GetCardExample {\n\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n		clientCards.setUserId(\"ba.greco.33\");\n		\n		try {\n			Card card = cardsApi.getCard(\"1015\");\n			System.out.println(card);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c3\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n    \nCardsApi *cardsApi = [[CardsApi alloc]init]; \n\n[cardsApi getCardWithCompletionBlock:@\"1015\" userId:@\"ba.greco.33\" completionHandler:^(CardsCard *output, NSError *error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>GET /cards/v1/1015?userId=ba.greco.33</p>
///  @param cardID Card identifier
///
///  @param userId User unique identifier
///
///  @returns CardsCard*
///
-(NSNumber*) getCardWithCompletionBlock: (NSString*) cardID
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsCard* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `getCard`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsCard*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsCard*)data, error);
              }
          ];
}

///
/// Updates card agreements
/// <p style=\"font-size: 14px;\">Updates the agreements of a specific credit card. The info that can be updated are the billing type (BALANCE or REVOLVING), the billing day and the installment amount (if the billing type is REVOLVING).</p> <h2>Optional Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">billingType</td>          <td align=\"left\">Card repay type (e.g. Balance, Revolving)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">billingDay</td>          <td align=\"left\">The day of the month in which the residual debit must be paid</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentAmount</td>          <td align=\"left\">The installment amount to be paid every month by a revolving credit card</td>       </tr>    </tbody> </table> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">cardId</td>          <td align=\"left\">Card PAN code</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">name</td>          <td align=\"left\">card name (e.g. Flexia)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">issueDate</td>          <td align=\"left\">Card issue date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">expirationDate</td>          <td align=\"left\">Card expiration date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">plafond</td>          <td align=\"left\">Card plafond amount. If the card is prepaid the plafond is the amount ceiling of the card, else is the monthly expense ceiling</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">balance</td>          <td align=\"left\">Card balance amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">holderName</td>          <td align=\"left\">Card holder full name</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">holderIdentifier</td>          <td align=\"left\">Card holder unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">productType</td>          <td align=\"left\">Card product type (e.g. Credit Card)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">status</td>          <td align=\"left\">Card status (e.g. ACTIVE, CANCELLED)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">billingType</td>          <td align=\"left\">Card repay type (e.g. Balance, Revolving)</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">billingDay</td>          <td align=\"left\">Card billing day</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentAmount</td>          <td align=\"left\">Card installment amount  if the card billing type is REVOLVING</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a update card agreements API call with Java and Objective-C SDK.</p> <div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java8\">\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.Card;\nimport eu.unicredit.api.client.cards.model.CardAgreements;\nimport eu.unicredit.api.client.cards.model.CardAgreements.BillingTypeEnum;\nimport eu.unicredit.api.client.cards.model.CardAgreementsPut;\n\npublic class UpdateCardAgreementExample {\n	\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n\n		try {\n			CardAgreementsPut request = new CardAgreementsPut();\n			CardAgreements cardAgreements = new CardAgreements();\n			cardAgreements.setBillingType(BillingTypeEnum.BALANCE);\n			request.setCardAgreements(cardAgreements);\n			Card cardsList = cardsApi.updateCard(\"4\", request);\n			System.out.println(cardsList);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c8\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n    \nCardsApi *cardsApi = [[CardsApi alloc]init];\n\nCardsCardAgreementsPut *cardsrequest = [[CardsCardAgreementsPut alloc] init];\nCardsCardAgreements * cardAgreement = [[CardsCardAgreements alloc] init];\n[cardAgreement setBillingType:@\"BALANCE\"];\n[cardsrequest setCardAgreements:cardAgreement];\n\n[cardsApi updateCardWithCompletionBlock:@\"4\" body:cardsrequest userId:nil completionHandler:^(CardsCard *output, NSError *error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>PUT /cards/v1/4</p>\n<pre class=\"code-snippet\" style=\"width: 75%;\">\n{\n	\"cardAgreements\": {\n		\"billingType\": \"BALANCE\"\n	}\n}\n</pre>
///  @param cardID Card identifier
///
///  @param body New card agreements object
///
///  @param userId User unique identifier
///
///  @returns CardsCard*
///
-(NSNumber*) updateCardWithCompletionBlock: (NSString*) cardID
         body: (CardsCardAgreementsPut*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsCard* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `updateCard`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `updateCard`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"PUT"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsCard*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsCard*)data, error);
              }
          ];
}

///
/// Retrieves the activities of a specific credit card
/// <p style=\"font-size: 14px;\">Retrieves the activities list associated to a specific credit card. It's possible to filter the result set specifing the following filters in the query string: from - to (time interval), gt - lt (amount interval), activity status, activity type.</p> <h2>Optional Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">from</td>          <td align=\"left\">Querystring parameter to get only the card activities occurred after the date specified</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">to</td>          <td align=\"left\">Querystring parameter to get only the card activities occurred before the date specified</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">gt</td>          <td align=\"left\">Querystring parameter to get the card activities with an amount greater than specified</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">lt</td>          <td align=\"left\">Querystring parameter to get the card activities with an amount lower than specified</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">status</td>          <td align=\"left\">Querystring parameter to get the card activities with the status specified.</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">type</td>          <td align=\"left\">Querystring parameter to get the card activities of the type specified.</td>       </tr>    </tbody> </table> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">activityId</td>          <td align=\"left\">Activity unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">amount</td>          <td align=\"left\">Activity amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">operationDate</td>          <td align=\"left\">Operation date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">valueDate</td>          <td align=\"left\">Date the amount was credited/debited to the card</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">description</td>          <td align=\"left\">Movement Description</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a get card activities API call with Java and Objective-C SDK.</p>\n<div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java4\">\nimport java.text.SimpleDateFormat;\nimport java.util.Date;\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.ActivitiesList;\n\npublic class GetCardActivitiesExample {\n\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n		clientCards.setUserId(\"mario.rossi\");\n\n		try {\n			Date operationDateFrom = new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2001-01-01\");\n			Date operationDateTo = new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2015-10-01\");\n			Date valueDateFrom = new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2001-01-01\");\n			Date valueDateTo = new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2015-10-01\");\n\n			ActivitiesList activities = cardsApi.getCardActivities(\"4\", 150.0, 8.0, operationDateFrom, operationDateTo,\n					valueDateFrom, valueDateTo);\n			System.out.println(activities);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c4\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n    \nCardsApi *cardsApi = [[CardsApi alloc]init];\n\nNSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];\n[dateFormat setDateFormat:@\"yyyy-MM-dd\"];\n\nNSDate *operationDateFrom = [dateFormat dateFromString:@\"2001-01-01\"];\nNSDate *operationDateTo = [dateFormat dateFromString:@\"2015-10-01\"];\nNSDate *valueDateFrom = [dateFormat dateFromString:@\"2001-01-01\"];\nNSDate *valueDateTo = [dateFormat dateFromString:@\"2015-10-01\"];\n\n[cardsApi getCardActivitiesWithCompletionBlock:@\"4\" lt:@\"150.0\" gt:@\"8.0\" valueDateFrom:operationDateFrom valueDateTo:operationDateTo operationDateFrom:valueDateFrom operationDateTo:valueDateTo userId:@\"mario.rossi\" completionHandler:^(CardsActivitiesList *output, NSError *error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>GET /cards/v1/4/activities?lt=150.0&gt=8.0&operationDateFrom=2001-01-01&operationDateTo=2015-10-01&valueDateFrom=2001-01-01&valueDateTo=2015-10-01&userId=mario.rossi</p>
///  @param cardID Card identifier
///
///  @param amountLT Less than amount
///
///  @param amountGT Greater than amount
///
///  @param valueDateFrom Activity value date from
///
///  @param valueDateTo Activity value date to
///
///  @param operationDateFrom Activity operation date from
///
///  @param operationDateTo Activity operation date to
///
///  @param userId User unique identifier
///
///  @returns CardsActivitiesList*
///
-(NSNumber*) getCardActivitiesWithCompletionBlock: (NSString*) cardID
         amountLT: (NSNumber*) amountLT
         amountGT: (NSNumber*) amountGT
         valueDateFrom: (NSDate*) valueDateFrom
         valueDateTo: (NSDate*) valueDateTo
         operationDateFrom: (NSDate*) operationDateFrom
         operationDateTo: (NSDate*) operationDateTo
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsActivitiesList* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `getCardActivities`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}/activities"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(amountLT != nil) {
        
        queryParams[@"amountLT"] = amountLT;
    }
    if(amountGT != nil) {
        
        queryParams[@"amountGT"] = amountGT;
    }
    if(valueDateFrom != nil) {
        
        queryParams[@"valueDateFrom"] = valueDateFrom;
    }
    if(valueDateTo != nil) {
        
        queryParams[@"valueDateTo"] = valueDateTo;
    }
    if(operationDateFrom != nil) {
        
        queryParams[@"operationDateFrom"] = operationDateFrom;
    }
    if(operationDateTo != nil) {
        
        queryParams[@"operationDateTo"] = operationDateTo;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsActivitiesList*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsActivitiesList*)data, error);
              }
          ];
}

///
/// Retrieves the installment plans of a specific credit card
/// <p style=\"font-size: 14px;\">Retrieves the list of the installment plans for a specific credit card. Active or inactive installment plans are returned, based on the query string parameter \"active\". Set it to true in order to obtain the list of the active installment plans. Set it to false otherwise. It's also possible to specify the \"from\" and \"to\" query string parameters in order to indicate a specific time interval for the search.</p> <h2>Required Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">from</td>          <td align=\"left\">Querystring parameter to show only the transactions occurred after the date specified</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">to</td>          <td align=\"left\">Querystring parameter to show only the transactions occurred before the date specified</td>       </tr>    </tbody> </table> <h2>Optional Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">active</td>          <td align=\"left\">Querystring parameter to get the active/inactive installments plan. Possible values: 'true' or 'false'.</td>       </tr>    </tbody> </table> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentPlanId</td>          <td align=\"left\">Installment plan unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsAmount</td>          <td align=\"left\">Amount of every rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsNumber</td>          <td align=\"left\"> Rates number of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalAmount</td>          <td align=\"left\">Installment plan total amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movementsNumber</td>          <td align=\"left\">Installment plan movements number</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.activityId</td>          <td align=\"left\">Activity unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.amount</td>          <td align=\"left\">Activity amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.operationDate</td>          <td align=\"left\">Operation date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.valueDate</td>          <td align=\"left\">Value date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.pan</td>          <td align=\"left\">Card PAN code on which the movement is registered</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.description</td>          <td align=\"left\">Movement Description</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.installmentID</td>          <td align=\"left\">Installment unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.amount</td>          <td align=\"left\">Installment amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.date</td>          <td align=\"left\">Installment date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.fee</td>          <td align=\"left\">Installment fee</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">averageFees</td>          <td align=\"left\">Plan average fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalFees</td>          <td align=\"left\">Plan total fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">feesAmount</td>          <td align=\"left\">Plan fees amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">taeg</td>          <td align=\"left\">the interest rate percentage that counts all the expenses besides the interest</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">teg</td>          <td align=\"left\">Gross percentage rate</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">firstInstallment</td>          <td align=\"left\">Date of the first rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">lastInstallment</td>          <td align=\"left\">Date of the last rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">residualDebit</td>          <td align=\"left\">Residual amount to be paid</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a get installment plans API call with Java and Objective-C SDK.</p>\n<div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java6\">\nimport java.text.SimpleDateFormat;\nimport java.util.Date;\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.InstallmentPlansList;\n\npublic class GetCardInstallmentsExample {\n	\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n		\n		try {\n			Date to = new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2017-01-01\");\n			Date from = new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2001-01-01\");\n			InstallmentPlansList planList = cardsApi.getCardInstallmentsPlan(\"true\", \"4\", to, from);\n			System.out.println(planList);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c6\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n    \nCardsApi *cardsApi = [[CardsApi alloc]init];\n\nNSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];\n[dateFormat setDateFormat:@\"yyyy-MM-dd\"]; \n\nNSDate *from = [dateFormat dateFromString:@\"2001-01-01\"];\nNSDate *to = [dateFormat dateFromString:@\"2017-01-01\"];\n\n[cardsApi getCardInstallmentsPlanWithCompletionBlock:@\"true\" cardID:@\"4\" to:to from:from userId:nil completionHandler:^(CardsInstallmentPlansList *output, NSError *error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>GET /cards/v1/4/installmentplans?active=true&from=2001-01-01&to=2017-01-01</p>
///  @param active Active installment plan filter (true or false)
///
///  @param cardID Card identifier
///
///  @param to Installment plan date to
///
///  @param from Installment plan date from
///
///  @param userId User unique identifier
///
///  @returns CardsInstallmentPlansList*
///
-(NSNumber*) getCardInstallmentsPlanWithCompletionBlock: (NSString*) active
         cardID: (NSString*) cardID
         to: (NSDate*) to
         from: (NSDate*) from
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsInstallmentPlansList* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'active' is set
    if (active == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `active` when calling `getCardInstallmentsPlan`"];
    }
    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `getCardInstallmentsPlan`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}/installmentplans"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(to != nil) {
        
        queryParams[@"to"] = to;
    }
    if(from != nil) {
        
        queryParams[@"from"] = from;
    }
    if(active != nil) {
        
        queryParams[@"active"] = active;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsInstallmentPlansList*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsInstallmentPlansList*)data, error);
              }
          ];
}

///
/// Creates a new installment plan
/// <p style=\"font-size: 14px;\">Creates a new installment plan. The installments number and the movements list must be specified in the request body. A new installment plan is created with a certain number of installments associated. The first installment will be paid on the current month, the following are spaced by a month. An operation preview is also provided by the \"preview\" filter.</p> <h2>Required Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">preview</td>          <td align=\"left\">Querystring parameter to specify if a preview of the operation is requested</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsNumber</td>          <td align=\"left\">The number of the installments of the new plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">Movements</td>          <td align=\"left\">The list of the movements to be included into the new plan</td>       </tr>    </tbody> </table> <p style=\"font-size: 14px;\">For each movement object, the following parameters must be specified:</p> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">activityId</td>          <td align=\"left\">Activity unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">amount</td>          <td align=\"left\">Activity amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">operationDate</td>          <td align=\"left\">Operation date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">valueDate</td>          <td align=\"left\">Date the amount was credited/debited to the card</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">description</td>          <td align=\"left\">Movement Description</td>       </tr>    </tbody> </table> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentPlanId</td>          <td align=\"left\">Installment plan unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsAmount</td>          <td align=\"left\">Amount of every rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsNumber</td>          <td align=\"left\"> Rates number of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalAmount</td>          <td align=\"left\">Installment plan total amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movementsNumber</td>          <td align=\"left\">Installment plan movements number</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.activityId</td>          <td align=\"left\">Activity unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.amount</td>          <td align=\"left\">Activity amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.operationDate</td>          <td align=\"left\">Operation date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.valueDate</td>          <td align=\"left\">Value date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.pan</td>          <td align=\"left\">Card PAN code on which the movement is registered</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.description</td>          <td align=\"left\">Movement Description</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.installmentID</td>          <td align=\"left\">Installment unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.amount</td>          <td align=\"left\">Installment amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.date</td>          <td align=\"left\">Installment date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.fee</td>          <td align=\"left\">Installment fee</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">averageFees</td>          <td align=\"left\">Plan average fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalFees</td>          <td align=\"left\">Plan total fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">feesAmount</td>          <td align=\"left\">Plan fees amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">taeg</td>          <td align=\"left\">the interest rate percentage that counts all the expenses besides the interest</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">teg</td>          <td align=\"left\">Gross percentage rate</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">firstInstallment</td>          <td align=\"left\">Date of the first rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">lastInstallment</td>          <td align=\"left\">Date of the last rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">residualDebit</td>          <td align=\"left\">Residual amount to be paid</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a create installment plan API call with Java and Objective-C SDK.</p> <div> <div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java1\">\nimport java.text.SimpleDateFormat;\nimport java.util.ArrayList;\nimport java.util.List;\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.InstallmentPlan;\nimport eu.unicredit.api.client.cards.model.NewInstallmentPlan;\nimport eu.unicredit.api.client.cards.model.Activity;\n\npublic class CreateCardInstallmentExample {\n\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n\n		try {\n			NewInstallmentPlan request = new NewInstallmentPlan();\n			request.setInstallmentsNumber(4);\n			List<Activity> movements = new ArrayList<Activity>();\n			Activity activity = new Activity();\n			activity.setActivityID(2365);\n			activity.setAmount(30.0);\n			activity.setValueDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2011-01-01\"));\n			activity.setOperationDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2011-01-01\"));\n			movements.add(activity);\n			activity.setActivityID(456);\n			activity.setAmount(50.0);\n			activity.setValueDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2011-01-01\"));\n			activity.setOperationDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2011-01-01\"));\n			movements.add(activity);\n			request.setMovements(movements);\n			InstallmentPlan plan = cardsApi.createCardInstallmentPlan(\"false\", \"4\", request);\n			System.out.println(plan);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}	\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c1\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n\nCardsApi *cardsApi = [[CardsApi alloc]init];\n\nCardsNewInstallmentPlan * request = [[CardsNewInstallmentPlan alloc] init];\n[request setInstallmentsNumber:[NSNumber numberWithInt:4]];\nNSMutableArray<CardsActivity> *movements = [[NSMutableArray<CardsActivity> alloc]init];\nCardsActivity * activity = [[CardsActivity alloc] init];\n[activity setActivityID:[NSNumber numberWithInt:2365]];\n[activity setAmount:[NSNumber numberWithDouble:30.0]];\n\nNSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];\n[dateFormat setDateFormat:@\"yyyy-MM-dd\"];\n\nNSDate *valueDate = [dateFormat dateFromString:@\"2011-01-01\"];\nNSDate *operationDate = [dateFormat dateFromString:@\"2011-01-01\"];\n\n[activity setValueDate:valueDate];\n[activity setOperationDate:operationDate];\n\n[movements addObject:activity];\n\nCardsActivity *activity2 = [CardsActivity new];\n\n\n[activity2 setActivityID:[NSNumber numberWithInt:456]];\n[activity2 setAmount:[NSNumber numberWithDouble:50.0]];\n[activity2 setValueDate:valueDate];\n[activity2 setOperationDate:operationDate];\n\n[movements addObject:activity2];\n\n[request setMovements:movements];\n\n[cardsApi createCardInstallmentPlanWithCompletionBlock:@\"false\" cardID:@\"4\" body:request userId:nil completionHandler:^(CardsInstallmentPlan output, NSError error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>POST /cards/v1/4/installmentplans?preview=false</p>\n<pre class=\"code-snippet\" style=\"width:75%\">\n{\n	\"installmentsNumber\": 4,\n	\"movements\": [{\n		\"activityID\": 2365,\n		\"amount\": 30.0,\n		\"operationDate\": \"2015-10-28\",\n		\"valueDate\": \"2015-10-29\"\n	},\n	{\n		\"activityID\": 456,\n		\"amount\": 50.0,\n		\"operationDate\": \"2015-10-15\",\n		\"valueDate\": \"2015-10-16\"\n	}]\n}\n</pre>
///  @param preview If true, an installment plan preview is requested
///
///  @param cardID Card identifier
///
///  @param body New installment plan object
///
///  @param userId User unique identifier
///
///  @returns CardsInstallmentPlan*
///
-(NSNumber*) createCardInstallmentPlanWithCompletionBlock: (NSString*) preview
         cardID: (NSString*) cardID
         body: (CardsNewInstallmentPlan*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsInstallmentPlan* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'preview' is set
    if (preview == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `preview` when calling `createCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `createCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `createCardInstallmentPlan`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}/installmentplans"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(preview != nil) {
        
        queryParams[@"preview"] = preview;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"POST"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsInstallmentPlan*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsInstallmentPlan*)data, error);
              }
          ];
}

///
/// Retrieves a specific installment plan
/// <p style=\"font-size: 14px;\">Retrieves the detail of a specific installment plan. This API returns several informations about the installment plan, including the details of the movements list whose total amoun is splitted into the installments.</p> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentPlanId</td>          <td align=\"left\">Installment plan unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsAmount</td>          <td align=\"left\">Amount of every rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsNumber</td>          <td align=\"left\"> Rates number of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalAmount</td>          <td align=\"left\">Installment plan total amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movementsNumber</td>          <td align=\"left\">Installment plan movements number</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.activityId</td>          <td align=\"left\">Activity unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.amount</td>          <td align=\"left\">Activity amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.operationDate</td>          <td align=\"left\">Operation date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.valueDate</td>          <td align=\"left\">Value date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.pan</td>          <td align=\"left\">Card PAN code on which the movement is registered</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.description</td>          <td align=\"left\">Movement Description</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.installmentID</td>          <td align=\"left\">Installment unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.amount</td>          <td align=\"left\">Installment amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.date</td>          <td align=\"left\">Installment date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.fee</td>          <td align=\"left\">Installment fee</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">averageFees</td>          <td align=\"left\">Plan average fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalFees</td>          <td align=\"left\">Plan total fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">feesAmount</td>          <td align=\"left\">Plan fees amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">taeg</td>          <td align=\"left\">the interest rate percentage that counts all the expenses besides the interest</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">teg</td>          <td align=\"left\">Gross percentage rate</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">firstInstallment</td>          <td align=\"left\">Date of the first rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">lastInstallment</td>          <td align=\"left\">Date of the last rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">residualDebit</td>          <td align=\"left\">Residual amount to be paid</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a get installment plan API call with Java and Objective-C SDK.</p>\n<div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java5\">\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.InstallmentPlan;\n\npublic class GetCardInstallmentPlanExample {\n	\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n\n		try {\n			InstallmentPlan plan = cardsApi.getCardInstallmentPlan(\"4\", \"15\");\n			System.out.println(plan);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c5\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n    \nCardsApi *cardsApi = [[CardsApi alloc]init];\n\n[cardsApi getCardInstallmentPlanWithCompletionBlock:@\"4\" planID:@\"15\" userId:nil completionHandler:^(CardsInstallmentPlan *output, NSError *error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>GET /cards/v1/4/installmentplans/15</p>
///  @param cardID Card identifier
///
///  @param planID Installment plan identifier
///
///  @param userId User unique identifier
///
///  @returns CardsInstallmentPlan*
///
-(NSNumber*) getCardInstallmentPlanWithCompletionBlock: (NSString*) cardID
         planID: (NSString*) planID
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsInstallmentPlan* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `getCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'planID' is set
    if (planID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `planID` when calling `getCardInstallmentPlan`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}/installmentplans/{planID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    if (planID != nil) {
        pathParams[@"planID"] = planID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"GET"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsInstallmentPlan*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsInstallmentPlan*)data, error);
              }
          ];
}

///
/// Updates an installment plan
/// <p style=\"font-size: 14px;\">Updates an existing inactive installment plan. It's possible to update an inactive installment plan, choosing for example a different installments number or a different movements list to be included into the plan. The installments number and the movements list must be specified in the request body. The result is a new installment plan with a new Id. An operation preview is also provided by the \"preview\" filter.</p> <h2>Required Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">preview</td>          <td align=\"left\">Querystring parameter to specify if a preview of the operation is requested</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsNumber</td>          <td align=\"left\">The number of the installments of the new plan</td>       </tr>    </tbody> </table> <h2>Optional Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">Movements</td>          <td align=\"left\">The list of the movements to be included into the new plan</td>       </tr>    </tbody> </table> <p style=\"font-size: 14px;\">For each Movement object, the following parameters must be specified:</p> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">activityId</td>          <td align=\"left\">Activity unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">amount</td>          <td align=\"left\">Activity amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">operationDate</td>          <td align=\"left\">Operation date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">valueDate</td>          <td align=\"left\">Date the amount was credited/debited to the card</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">description</td>          <td align=\"left\">Movement Description</td>       </tr>    </tbody> </table> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentPlanId</td>          <td align=\"left\">Installment plan unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsAmount</td>          <td align=\"left\">Amount of every rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installmentsNumber</td>          <td align=\"left\"> Rates number of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalAmount</td>          <td align=\"left\">Installment plan total amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movementsNumber</td>          <td align=\"left\">Installment plan movements number</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.activityId</td>          <td align=\"left\">Activity unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.amount</td>          <td align=\"left\">Activity amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.operationDate</td>          <td align=\"left\">Operation date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.valueDate</td>          <td align=\"left\">Value date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.pan</td>          <td align=\"left\">Card PAN code on which the movement is registered</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">movements.description</td>          <td align=\"left\">Movement Description</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.installmentID</td>          <td align=\"left\">Installment unique identifier</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.amount</td>          <td align=\"left\">Installment amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.date</td>          <td align=\"left\">Installment date</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">installments.fee</td>          <td align=\"left\">Installment fee</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">averageFees</td>          <td align=\"left\">Plan average fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">totalFees</td>          <td align=\"left\">Plan total fees</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">feesAmount</td>          <td align=\"left\">Plan fees amount</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">taeg</td>          <td align=\"left\">the interest rate percentage that counts all the expenses besides the interest</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">teg</td>          <td align=\"left\">Gross percentage rate</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">firstInstallment</td>          <td align=\"left\">Date of the first rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">lastInstallment</td>          <td align=\"left\">Date of the last rate of the plan</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">residualDebit</td>          <td align=\"left\">Residual amount to be paid</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a update installment plan API call with Java and Objective-C SDK.</p> <div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java9\">\nimport java.text.SimpleDateFormat;\nimport java.util.ArrayList;\nimport java.util.List;\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.Activity;\nimport eu.unicredit.api.client.cards.model.InstallmentPlan;\nimport eu.unicredit.api.client.cards.model.NewInstallmentPlan;\n\npublic class UpdateCardInstallmentExample {\n	\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n		clientCards.setUserId(\"mario.rossi\");\n		\n		try {\n			NewInstallmentPlan request = new NewInstallmentPlan();\n			request.setInstallmentsNumber(3);\n			List<Activity> movements = new ArrayList<Activity>();\n			Activity activity = new Activity();\n			activity.setActivityID(2365);\n			activity.setAmount(30.0);\n			activity.setOperationDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2011-01-01\"));\n			activity.setValueDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2012-01-01\"));\n			movements.add(activity);\n			activity.setActivityID(456);\n			activity.setAmount(50.0);\n			activity.setOperationDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2013-01-01\"));\n			activity.setValueDate(new SimpleDateFormat(\"yyyy-MM-dd\").parse(\"2014-01-01\"));\n			movements.add(activity);\n			request.setMovements(movements);\n			InstallmentPlan plan = cardsApi.updateCardInstallmentPlan(\"4\", \"false\", \"19\", request);\n			System.out.println(plan);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c9\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n\nCardsApi *cardsApi = [[CardsApi alloc]init];\n\nCardsNewInstallmentPlan *requestInstPlan = [[CardsNewInstallmentPlan alloc] init];\n[requestInstPlan setInstallmentsNumber:[NSNumber numberWithInt:3]];\n\nNSMutableArray<CardsActivity> *movements = [[NSMutableArray<CardsActivity> alloc]init];\n\nCardsActivity * activity = [[CardsActivity alloc] init];\n[activity setActivityID:@\"2365\"];\n[activity setAmount:[NSNumber numberWithDouble:30.0]];\n\nNSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];\n[dateFormat setDateFormat:@\"yyyy-MM-dd\"]; \n\nNSDate *valueDate = [dateFormat dateFromString:@\"2011-01-01\"];\nNSDate *operationDate = [dateFormat dateFromString:@\"2012-01-01\"];\n\n[activity setValueDate:valueDate];\n[activity setOperationDate:operationDate];\n\n[movements addObject:activity];\n\n[activity setActivityID:@\"456\"];\n[activity setAmount:[NSNumber numberWithDouble:50.0]];\n\nNSDate *valueDate2 = [dateFormat dateFromString:@\"2014-01-01\"];\nNSDate *operationDate2 = [dateFormat dateFromString:@\"2013-01-01\"];\n\n[activity setValueDate:valueDate2];\n[activity setOperationDate:operationDate2];\n\n[movements addObject:activity];\n\n[requestInstPlan setMovements:movements];\n\n[cardsApi updateCardInstallmentPlanWithCompletionBlock:@\"4\" preview:@\"false\" planID:@\"19\" body:requestInstPlan userId:@\"mario.rossi\" completionHandler:^(CardsInstallmentPlan *output, NSError *error) {\n	if (output) {	\n		NSLog(@\"description : %@\", output );\n		\n	} else {	\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);		\n	}	\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>PUT /cards/v1/4/installmentplans/19?preview=false&userId=mario.rossi</p>\n<pre class=\"code-snippet\" style=\"width: 75%;\">\n{\n	\"installmentsNumber\": 3,\n	\"movements\": [{\n		\"activityID\": 2365,\n		\"amount\": 30.0,\n		\"operationDate\": \"2015-11-03\",\n		\"valueDate\": \"2011-11-14\"\n	},\n	{\n		\"activityID\": 456,\n		\"amount\": 50.0,\n		\"operationDate\": \"2015-11-01\",\n		\"valueDate\": \"2014-11-02\"\n	}]\n}\n</pre>
///  @param cardID Card identifier
///
///  @param preview Preview requested (true, false)
///
///  @param planID Installment plan identifier
///
///  @param body Updated installment plan object
///
///  @param userId User unique identifier
///
///  @returns CardsInstallmentPlan*
///
-(NSNumber*) updateCardInstallmentPlanWithCompletionBlock: (NSString*) cardID
         preview: (NSString*) preview
         planID: (NSString*) planID
         body: (CardsNewInstallmentPlan*) body
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsInstallmentPlan* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `updateCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'preview' is set
    if (preview == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `preview` when calling `updateCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'planID' is set
    if (planID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `planID` when calling `updateCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'body' is set
    if (body == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `body` when calling `updateCardInstallmentPlan`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}/installmentplans/{planID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    if (planID != nil) {
        pathParams[@"planID"] = planID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(preview != nil) {
        
        queryParams[@"preview"] = preview;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[@"application/json"]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    bodyParam = body;
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"PUT"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsInstallmentPlan*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsInstallmentPlan*)data, error);
              }
          ];
}

///
/// Cancels or extinguishes an installment plan
/// <p style=\"font-size: 14px;\">Cancels or extinguishes an existing installment plan. It's possible to cancel an inactive installment plan (an installment plan that isn't already started) or to extinguish an active installment plan (an installment plan where some installments has already been paid), welding the residual debit in a single last installment. In order to cancel or extinguish an installment plan, you have to specify the query string parameter \"operation\" with the value \"C\" or \"E\" respectively. It's also possible to request an operation preview, setting the query string parameter \"preview\" to the \"true\" value.</p> <h2>Required Parameters</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">operation</td>          <td align=\"left\">Querystring paramenter to specify which is the operation type, 'C' or 'E'</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">preview</td>          <td align=\"left\">Querystring parameter to specify if a preview of the operation is requested</td>       </tr>    </tbody> </table> <h2>Response Fields</h2> <table style=\"border: 2px solid rgb(195, 232, 209); margin-top: 20px; margin-bottom: 20px; width: 75%;\">    <thead>       <tr>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">PROPERTY</th>          <th align=\"left\" style=\"font-size:20px;background-color:#E1E1E1;\">DESCRIPTION</th>       </tr>    </thead>    <tbody>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">resisualAmount</td>          <td align=\"left\">Residual amount to be paid</td>       </tr>       <tr>          <td align=\"left\" style=\"border: 1px solid rgb(195, 232, 209);font-family: Courier New;\">effectiveDate</td>          <td align=\"left\">Date of the last rate of the plan</td>       </tr>    </tbody> </table> <h2>Example</h2> <p style=\"font-size: 14px;\">The example below shows a delete installment plan API call with Java and Objective-C SDK.</p>\n<div>\n<pre class=\"prettyprint lang-java\" name=\"Java\" id=\"java2\">\nimport eu.unicredit.api.client.ApiClient;\nimport eu.unicredit.api.client.ApiException;\nimport eu.unicredit.api.client.cards.CardsApi;\nimport eu.unicredit.api.client.cards.model.DeleteInstallmentPlan;\n\npublic class DeleteCardInstallmentExample {\n\n	public static void main(String[] args) throws Exception {\n\n		ApiClient clientCards = new ApiClient();\n		clientCards.setBasePath(\"https://ucg-apimanager.axwaycloud.net:8065\");\n		clientCards.setApiKey(\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\");\n		CardsApi cardsApi = new CardsApi(clientCards);\n\n		try {\n			DeleteInstallmentPlan plan = cardsApi.deleteCardInstallmentPlan(\"4\", \"false\", \"E\", \"14\");\n			System.out.println(plan);\n		} catch (ApiException e) {\n			System.out.println(e.getHttpError());\n			System.out.println(e.getErrorCode());\n			System.out.println(e.getErrorMessage());\n			System.out.println(e.getMoreInfo());\n		}\n	}\n}\n</pre>\n<pre class=\"prettyprint lang-m\" name=\"Objective-C\" id=\"object_c2\">\n#import <CardsApi.h>\n\n[[CardsConfiguration sharedConfig] setApiKey:@\"7187ac1b-e868-4009-bdb7-c321fc5d6e71\" forApiKeyIdentifier:@\"keyId\"];\n    \nCardsApi *cardsApi = [[CardsApi alloc]init]; \n\n[cardsApi deleteCardInstallmentPlanWithCompletionBlock:@\"4\" preview:@\"false\" operation:@\"E\" planID:@\"14\" userId:nil completionHandler:^(CardsDeleteInstallmentPlan *output, NSError *error) {\n	if (output) {\n		NSLog(@\"description : %@\", output );\n	} else {\n		NSLog(@\"error: %@\", [[error userInfo] objectForKey:(@\"CardsResponseObject\")]);\n	}\n}];\n</pre>\n</div>\n<h2>Sample Request</h2>\n<p>DELETE /cards/v1/4/installmentplans/14?operation=E&preview=false</p>
///  @param cardID Card identifier to which the plan is applied
///
///  @param preview Preview requested (true, false)
///
///  @param operation Operation code. C for Cancel, E for extinguish.
///
///  @param planID Installment plan identifier
///
///  @param userId User unique identifier
///
///  @returns CardsDeleteInstallmentPlan*
///
-(NSNumber*) deleteCardInstallmentPlanWithCompletionBlock: (NSString*) cardID
         preview: (NSString*) preview
         operation: (NSString*) operation
         planID: (NSString*) planID
         userId: (NSString*) userId
        
        completionHandler: (void (^)(CardsDeleteInstallmentPlan* output, NSError* error))completionBlock { 
        

    
    // verify the required parameter 'cardID' is set
    if (cardID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `cardID` when calling `deleteCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'preview' is set
    if (preview == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `preview` when calling `deleteCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'operation' is set
    if (operation == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `operation` when calling `deleteCardInstallmentPlan`"];
    }
    
    // verify the required parameter 'planID' is set
    if (planID == nil) {
        [NSException raise:@"Invalid parameter" format:@"Missing the required parameter `planID` when calling `deleteCardInstallmentPlan`"];
    }
    

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/cards/v1/{cardID}/installmentplans/{planID}"];

    // remove format in URL if needed
    if ([resourcePath rangeOfString:@".{format}"].location != NSNotFound) {
        [resourcePath replaceCharactersInRange: [resourcePath rangeOfString:@".{format}"] withString:@".json"];
    }

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (cardID != nil) {
        pathParams[@"cardID"] = cardID;
    }
    if (planID != nil) {
        pathParams[@"planID"] = planID;
    }
    

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if(preview != nil) {
        
        queryParams[@"preview"] = preview;
    }
    if(operation != nil) {
        
        queryParams[@"operation"] = operation;
    }
    if(userId != nil) {
        
        queryParams[@"userId"] = userId;
    }
    
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.defaultHeaders];

    

    // HTTP header `Accept`
    headerParams[@"Accept"] = [CardsApiClient selectHeaderAccept:@[@"application/json"]];
    if ([headerParams[@"Accept"] length] == 0) {
        [headerParams removeObjectForKey:@"Accept"];
    }

    // response content type
    NSString *responseContentType;
    if ([headerParams objectForKey:@"Accept"]) {
        responseContentType = [headerParams[@"Accept"] componentsSeparatedByString:@", "][0];
    }
    else {
        responseContentType = @"";
    }

    // request content type
    NSString *requestContentType = [CardsApiClient selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[@"_default"];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *files = [[NSMutableDictionary alloc] init];
    
    
    

    
    return [self.apiClient requestWithCompletionBlock: resourcePath
                                               method: @"DELETE"
                                           pathParams: pathParams
                                          queryParams: queryParams
                                           formParams: formParams
                                                files: files
                                                 body: bodyParam
                                         headerParams: headerParams
                                         authSettings: authSettings
                                   requestContentType: requestContentType
                                  responseContentType: responseContentType
                                         responseType: @"CardsDeleteInstallmentPlan*"
                                      completionBlock: ^(id data, NSError *error) {
                  
                  completionBlock((CardsDeleteInstallmentPlan*)data, error);
              }
          ];
}



@end

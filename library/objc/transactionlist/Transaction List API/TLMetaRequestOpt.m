#import "TLMetaRequestOpt.h"

@implementation TLMetaRequestOpt

/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper
{
  return [[JSONKeyMapper alloc] initWithDictionary:@{ @"commentValue": @"commentValue", @"tags": @"tags", @"socialAccountID": @"socialAccountID", @"socialType": @"socialType", @"latitude": @"latitude", @"longitude": @"longitude", @"address": @"address", @"city": @"city", @"zipCode": @"zipCode", @"province": @"province", @"country": @"country" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
  NSArray *optionalProperties = @[@"commentValue", @"tags", @"socialAccountID", @"socialType", @"latitude", @"longitude", @"address", @"city", @"zipCode", @"province", @"country"];

  if ([optionalProperties containsObject:propertyName]) {
    return YES;
  }
  else {
    return NO;
  }
}

/**
 * Gets the string presentation of the object.
 * This method will be called when logging model object using `NSLog`.
 */
- (NSString *)description {
    return [[self toDictionary] description];
}

@end
